(function() {
  'use strict';

  angular
    .module('wud.techtest')
    .controller('wudNavbarController', wudNavbarController);

  /** @ngInject */
  function wudNavbarController($state, $filter) {
    var vm = this;
    vm.menu = $filter('filter')($state.get(), {abstract: '!true'})
  }
})();