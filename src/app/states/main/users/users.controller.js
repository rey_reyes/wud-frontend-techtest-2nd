(function() {
  'use strict';

  angular
    .module('wud.techtest')
    .controller('UsersController', UsersController);

  /** @ngInject */
  function UsersController(User, $state, $uibModal, $scope, $timeout) {
		var vm = this;
		vm.processing = false;
		User.query({},
		function(success){
			vm.list = success;
		});

		vm.add = function () {
			vm.button = true;
			vm.ok = false;
			vm.modalInstance = $uibModal.open({
        templateUrl: 'app/states/main/users/add.html',
        scope: $scope,
        size: 'md',
        backdrop: 'static',
        keyboard  : false
      });
		};

		vm.save = function () {
			if(vm.form.$invalid) return;
			vm.processing = true;
			User.save({},vm.newUser,
				function(){
					$timeout(function () {
						vm.button = false;
						vm.ok = true;
					}, 5000);
				});
		}

		vm.dismiss = function () {
			vm.modalInstance.dismiss('cancel');
			$state.reload();
		};
  }
})();