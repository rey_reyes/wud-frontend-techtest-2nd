(function(){
	'use strict';

	angular
		.module('wud.techtest')
		.factory('User', User)

	/** @ngInject */
	function User($resource){
		return $resource('http://localhost:8000/users');
	}
})();